import React, {useState} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import MyModal from "../components/MyModal";
import InputForm from "../components/InputForm";
import './App.css';

function App() {
    const [isResultModalVisible, setIsResultModalVisible] = useState(false);
    const [inputValue, setInputValue] = useState("");
    const [responseValue, setResponseValue] = useState("");
    const [errors, setErrors] = useState({});


    async function addParenthesesToNumbersApiCall() {
        try {
            const result = await fetch(`http://localhost:8080/api/add-parentheses-to-numbers/${inputValue}`);
            const resultText = await result.text();
            if (result.status === 200) {
                setResponseValue(resultText);
                showResultModal();
            } else {
                alert(resultText)
            }
        } catch (e) {
            alert(e);
        }
    }

    function isValidInput(value) {
        const isValid = value && !isNaN(value);
        setErrors(isValid
            ? {...errors, input: null}
            : {...errors, input: "Each character must be a decimal digit between 0 and 9, inclusive."});
        return isValid;
    }

    const onInputChange = (event) => {
        let value = event.target.value
        isValidInput(value) && setInputValue(value);
    }


    const handleSubmit = () => {
        isValidInput(inputValue) && addParenthesesToNumbersApiCall();
    };

    const showResultModal = () => {
        setIsResultModalVisible(true);
    };

    const closeResultModal = () => {
        setIsResultModalVisible(false);
    };

    return (
        <div>
            <h1 className={"text-center"}>Digital Lights Task</h1>
            <InputForm handleSubmit={handleSubmit}
                       isValid={isValidInput}
                       setInputValue={setInputValue}
                       errors={errors}
                       onInputChange={onInputChange}/>
            <MyModal isResultModalVisible={isResultModalVisible}
                     result={responseValue}
                     onHide={closeResultModal}>

            </MyModal>
        </div>
    )
}

export default App;