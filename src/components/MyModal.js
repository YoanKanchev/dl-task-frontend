import React from "react";

import {Modal} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const MyModal = ({isResultModalVisible, onHide, result}) => {

    return (
        <div>
            <Modal show={isResultModalVisible} onHide={() => onHide()}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        The result is:
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>{result}</p>
                </Modal.Body>
            </Modal>
        </div>)
}

export default MyModal;