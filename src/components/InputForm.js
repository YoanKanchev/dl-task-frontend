import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Col from 'react-bootstrap/Col';
import "./InputForm.css";

const InputForm = ({handleSubmit, errors, onInputChange}) => {
    return (
        <div className="Login">
            <Form  >
                <Form.Group as={Col} size="lg" controlId="validationCustom02">
                    <Form.Label>Provide decimal digit between 0 and 9.</Form.Label>
                    <Form.Control type="text" placeholder="Input"
                                  onChange={onInputChange}
                                  isInvalid={!!errors.input} />
                    <Form.Control.Feedback type="invalid">
                        {errors.input}
                    </Form.Control.Feedback>
                </Form.Group>
                <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', margin: 20}}>
                    <Button onClick={handleSubmit} disabled={!!errors.input}>
                        Submit
                    </Button>
                </div>
            </Form>
        </div>
    )
}
export default InputForm;
